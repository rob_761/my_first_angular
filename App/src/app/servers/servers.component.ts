import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: 'servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No server was created';   //string to store creation status
  serverName = 'test server';
  serverCreated = false;
  servers = ['Testserver', 'Testserver 2']; //array to hold servers

  constructor() {
    setTimeout(()=>{
      this.allowNewServer = true;
    },2000);
  }

  ngOnInit() {
  }
  //method to 'create' server
  onCreateServer() {
    this.serverCreated = true;
    this.servers.push(this.serverName); //add server to servers
    this.serverCreationStatus = 'Server was created, name is ' + this.serverName;
  }

  //method to update server name
  onUpdateServerName(event : Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }


}
