import {Component} from '@angular/core';


//Decorator
@Component({
  //metadata
  selector: 'app-server', //must be unique and not override default html elements
  templateUrl: './server.component.html',
  //adding the style class 'online' which is called by ngClass
  styles: [`
    .online {
      color: white;
    }
  `]
})
export class ServerComponent {
  serverId: number = 10;
  serverStatus: string = 'offline';

  constructor() {

    this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline'; //turnary
  }

  getServerStatus() {
    return this.serverStatus;
  }

  getColor() {
    return this.serverStatus === 'online' ? 'green' : 'red'; //green if serverStatus is online
  }
}
